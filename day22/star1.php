<?php

$input = file_get_contents('input.txt');

$deck = range(0, 119315717514046);

for ($iteration = 0; $iteration < 101741582076661; $iteration++) {

    foreach (explode("\n", $input) as $line) {
        if (preg_match('/^deal into new stack$/', $line, $output)) {
            $deck = deal($deck);
        } else if (preg_match('/^deal with increment ([\-0-9]+)$/', $line, $output)) {
            $deck = dealWithIncrement($deck, intval($output[1]));
        } else if (preg_match('/^cut ([\-0-9]+)$/', $line, $output)) {
            $deck = cut($deck, intval($output[1]));
        } else {
            die('uh oh. ' . $line);
        }
    }
}

var_dump($deck[2020]);

function deal($input) {
    return array_reverse($input);
}

function dealWithIncrement($input, $number) {
    $output = array_fill(0, sizeof($input), null);
    for ($i = 0, $c = sizeof($input); $i < $c; $i++) {
        $output[($i * $number) % sizeof($output)] = $input[$i];
    }

    return $output;
}

function cut($input, $number) {
    if ($number > 0) {
        $cut = array_splice($input, 0, $number);
        array_splice($input, sizeof($input), 0, $cut);
        return array_values($input);
    } else {
        $cut = array_splice($input, sizeof($input) + $number, abs($number));
        array_splice($input, 0, 0, $cut);
        return array_values($input);
    }
}