<?php

$input = file_get_contents(__DIR__ . '/input.txt');
$wires = explode("\n", $input);

$mapping = [
    'U' => [0, 1],
    'D' => [0, -1],
    'L' => [-1, 0],
    'R' => [1, 0],
];


$endpoints = [];

foreach ($wires as $wire) {
    $position = [0, 0];
    $wire_points = ['0,0'];
    $directions = explode(',', $wire);
    foreach ($directions as $direction) {
        $facing = substr($direction, 0, 1);
        $magnitude = intval(substr($direction, 1));

        for ($i = 1; $i <= $magnitude; $i++) {
            $position[0] += $mapping[$facing][0];
            $position[1] += $mapping[$facing][1];
            $wire_points[] = "{$position[0]},{$position[1]}";
        }
    }
    $endpoints[] = $wire_points;
}

$intersections = array_intersect($endpoints[0], $endpoints[1]);

foreach ($intersections as $intersection) {
    echo 'Distance of ' . $intersection . ': ';
    list($x, $y) = explode(',', $intersection);
    echo abs(intval($x)) + abs(intval($y));
    echo PHP_EOL;
}
