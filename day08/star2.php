<?php

require_once 'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;

// create an image manager instance with favored driver
$manager = new ImageManager(array('driver' => 'imagick'));

$input = file_get_contents('input.txt');

var_dump(strlen($input));

$raw = str_split($input, 25 * 6);
$layers = [];
foreach ($raw as $layer) {
    $layers[] = [
        'raw' => $layer,
    ];
}

$img = Image::canvas(25, 6, '#999999');

$overlay = array_fill(0, 25 * 6, null);

foreach ($layers as $layer) {
    foreach (str_split($layer['raw']) as $idx => $pixel) {
        if ($overlay[$idx] === null && $pixel != 2) {
            $overlay[$idx] = $pixel;
        }
    }
}

foreach ($overlay as $idx => $pixel) {
    $img->pixel($pixel == '0' ? '#111111' : '#FCFCFC', $idx % 25, floor($idx / 25));
}

$img->save('output.jpg');
