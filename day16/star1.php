<?php
ini_set('memory_limit', '16G');
$input = file_get_contents('input.txt');
$input = str_repeat($input, 10000);
$numbers = str_split($input);
$pattern = [0, 1, 0, -1];

for ($i = 1; $i <= 100; $i++) {
    $tmpNumbers = [];
    for ($n = 0, $c = count($numbers); $n < $c; $n++) {
        $tmpNumber = 0;
        $tmpPattern = getPattern($pattern, $n + 1);
        $countPattern = count($tmpPattern);
        for ($j = 0; $j < $c; $j++) {
            $tmpNumber += $numbers[$j] * $tmpPattern[($j + 1) % $countPattern];
        }
        $tmpNumbers[] = abs($tmpNumber) % 10;
    }
    echo "Iteration $i..." . PHP_EOL;
    $numbers = $tmpNumbers;
}

function getPattern($pattern, $repeat) {
    $response = [];
    foreach ($pattern as $value) {
        $response = $response + array_fill(count($response), $repeat, $value);
    }
    return $response;
}