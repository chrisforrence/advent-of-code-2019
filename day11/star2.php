<?php

require_once 'vendor/autoload.php';

// import the Intervention Image Manager Class
use Forrence\AdventOfCode\Intcode;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;

$computer = new Intcode;
$computer->setCode(explode(',', file_get_contents('./input.txt')));

$directions = [
    'N' => [0, 1],
    'E' => [1, 0],
    'S' => [0, -1],
    'W' => [-1, 0],
];

function turn($facing, $direction) {

    switch ($direction) {
        case 'L': // Turn left
            switch ($facing) {
                case 'N': return 'W';
                case 'E': return 'N';
                case 'S': return 'E';
                case 'W': return 'S';
            }
            break;
        case 'R': // Turn left
            switch ($facing) {
                case 'N': return 'E';
                case 'E': return 'S';
                case 'S': return 'W';
                case 'W': return 'N';
            }
            break;
    }
}

$map = ["0,0" => '1'];
$x = $y = 0;
$facing = 'N';
// Inputs  [{0: B, 1: W}]
// Outputs [{0: paint black, 1: paint white}, {0: left 90*, 1: right 90*}]
while (!$computer->isHalted()) {
    $coordinates = "{$x},{$y}";
    $color = $map[$coordinates] ?? '0';

    // Get paint color
    $computer->setInput([$color]);
    $computer->process();
    $computer->process();
    if (count($computer->getOutput())) {
        $map[$coordinates] = $computer->getOutput()[0];
        $facing = turn($facing, $computer->getOutput()[1] == 0 ? 'L' : 'R');

        $computer->setOutput([]);
        list($dx, $dy) = $directions[$facing];
        $x += $dx;
        $y += $dy;
    }
}
list($offsetX, $offsetY) = getCoordinatesOffset(array_keys($map));
list($maxX, $maxY) = getMaximumCoordinates(array_keys($map), $offsetX, $offsetY);

$img = Image::canvas($maxX, $maxY, '#999999');
foreach ($map as $coordinates => $color) {
    list($x, $y) = explode(',', $coordinates);
    $img->pixel($color == 1 ? '#FFFFFF' : '#111111', $x + $offsetX, $y + $offsetY);
}

$img->flip('v');
$img->save('output.png');


function getCoordinatesOffset($keys) {
    $x = $y = PHP_INT_MAX;
    foreach ($keys as $key) {
        list($cx, $cy) = explode(',', $key);
        $x = min($x, $cx);
        $y = min($y, $cy);
    }
    return [-1 * min($x, -10), -1 * min($y, -10)];
}
function getMaximumCoordinates($keys, $offsetX, $offsetY) {
    $x = $y = PHP_INT_MIN;
    foreach ($keys as $key) {
        list($cx, $cy) = explode(',', $key);
        $x = max($x, $cx);
        $y = max($y, $cy);
    }
    return [$x + $offsetX + 10, $y + $offsetY + 10];
}