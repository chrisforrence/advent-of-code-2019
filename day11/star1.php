<?php

require_once 'vendor/autoload.php';

use Forrence\AdventOfCode\Intcode;

$computer = new Intcode;
$computer->setCode(explode(',', file_get_contents('./input.txt')));

$directions = [
    'N' => [0, 1],
    'E' => [1, 0],
    'S' => [0, -1],
    'W' => [-1, 0],
];

function turn($facing, $direction) {

    switch ($direction) {
        case 'L': // Turn left
            switch ($facing) {
                case 'N': return 'W';
                case 'E': return 'N';
                case 'S': return 'E';
                case 'W': return 'S';
            }
            break;
        case 'R': // Turn left
            switch ($facing) {
                case 'N': return 'E';
                case 'E': return 'S';
                case 'S': return 'W';
                case 'W': return 'N';
            }
            break;
    }
}

$map = [];
$x = $y = 0;
$facing = 'N';
// Inputs  [{0: B, 1: W}]
// Outputs [{0: paint black, 1: paint white}, {0: left 90*, 1: right 90*}]
while (!$computer->isHalted()) {
    $coordinates = "{$x},{$y}";
    $color = $map[$coordinates] ?? '0';

    // Get paint color
    $computer->setInput([$color]);
    $computer->process();
    $computer->process();
    if (count($computer->getOutput())) {
        $map[$coordinates] = $computer->getOutput()[0];
        $facing = turn($facing, $computer->getOutput()[1] == 0 ? 'L' : 'R');

        $computer->setOutput([]);
        list($dx, $dy) = $directions[$facing];
        $x += $dx;
        $y += $dy;
    }
}

var_dump(count(array_keys($map)));