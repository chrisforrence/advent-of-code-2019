<?php

$input = file_get_contents('input.txt');
$rows = explode("\n", $input);
$potentials = [];

for ($y = 0, $countY = count($rows); $y < $countY; $y++) {
    $columns = str_split($rows[$y]);
    for ($x = 0, $countX = count($columns); $x < $countX; $x++) {
        if ($columns[$x] === '#') {
            $potentials[] = [
                'x' => $x,
                'y' => $y,
                'countVisible' => 0,
                'asteroids' => [],
            ];
        }
    }
}

for ($i = 0, $c = count($potentials); $i < $c; $i++) {
    $candidate = $potentials[$i];
    $ratios = [];
    foreach ($potentials as $asteroid) {
        if ($candidate['x'] === $asteroid['x'] && $candidate['y'] === $asteroid['y']) {
            continue;
        }
        $degree = getRatio($candidate['x'], $candidate['y'], $asteroid['x'], $asteroid['y']);
        $distance = getDistance($candidate['x'], $candidate['y'], $asteroid['x'], $asteroid['y']);
        if (!isset($ratios[$degree])) {
            $ratios[$degree] = [];
        }
        $ratios[$degree][$distance] = "{$asteroid['x']},{$asteroid['y']}";
    }
    foreach ($ratios as $degree => $distances) {
        ksort($distances);
        $ratios[$degree] = array_values($distances);
    }
    ksort($ratios);
    $ratios = array_values($ratios);

    $potentials[$i]['countVisible'] = count($ratios);
    $potentials[$i]['ratios'] = $ratios;
}

$station = null;
foreach ($potentials as $candidate) {
    if ($station === null || $candidate['countVisible'] > $station['countVisible']) {
        $station = $candidate;
    }
}

$vaporized = [];
var_dump($station);

while (!empty($station['ratios'])) {
    for ($angle = 0, $c = count($station['ratios']); $angle < $c; $angle++) {
        $vaporized[] = array_shift($station['ratios'][$angle]);
        if (empty($station['ratios'][$angle])) {
            $station['ratios'][$angle] = false;
        }
    }
    $station['ratios'] = array_values(array_filter($station['ratios']));
}
var_dump($vaporized);

function getRatio($startX, $startY, $endX, $endY) {
    $x = $startX - $endX;
    $y = $startY - $endY;

    return $x === 0 && $y === 0 ? 0 : number_format(abs(rad2deg(atan2($x, $y)) + ($x < 0 ? 360 : 0) - 360), 3);
}

function getDistance($startX, $startY, $endX, $endY) {
    $x = abs($endX - $startX);
    $y = abs($endY - $startY);
    return sqrt(pow($x, 2) + pow($y, 2));
}