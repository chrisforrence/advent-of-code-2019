use std::fs::File;
use std::io::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input.txt")?;
    let mut contents = String::new();
    let mut total = 0;

    file.read_to_string(&mut contents)?;

    for line in contents.lines() {
        let mass: i32 = line.parse().unwrap();
        total += get_fuel(mass);
    }

    println!("We need {} fuel units", total);
    Ok(())
}

fn get_fuel(mass: i32) -> i32 {
    let computed: i32 = ((mass as f32 / 3.0).floor() - 2.0) as i32;
    if computed <= 0 {
        return 0;
    }
    computed + get_fuel(computed)
}