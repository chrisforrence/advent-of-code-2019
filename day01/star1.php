<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$input = explode("\n", $input);
function getFuel($mass) {
    return floor($mass / 3) - 2;
}

$output = 0;

foreach ($input as $el) {
    $output += getFuel($el);
}

var_dump($output);