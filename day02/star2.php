<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$codes = explode(',', $input);

$desiredValue = 19690720;

for ($x = 0; $x <= 99; $x++) {
    for ($y = 0; $y <= 99; $y++) {
        if (check($codes, $x, $y) === 19690720) {
            var_dump($x . ', ' . $y);
            exit(0);
        }
    }
}

var_dump('Not found.');

// Custom
function check($codes, $input1, $input2) {

    $codes[1] = $input1;
    $codes[2] = $input2;

    for ($i = 0, $c = count($codes); $i < $c; $i+=4) {
        $opcode = $codes[$i];
        switch ($opcode) {
            case '1':
                $result = $codes[$codes[$i + 1]] + $codes[$codes[$i + 2]];

                $codes[$codes[$i + 3]] = $result;
                break;
            case '2':
                $result = $codes[$codes[$i + 1]] * $codes[$codes[$i + 2]];

                $codes[$codes[$i + 3]] = $result;
                break;
            case '99':
                break 2;
                break;
            default:
                echo 'wat. ' . $opcode . PHP_EOL;
                exit(1);
        }
    }

    return $codes[0];
}