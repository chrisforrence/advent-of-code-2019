<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$codes = explode(',', $input);

// Custom
$codes[1] = 12;
$codes[2] = 2;

for ($i = 0, $c = count($codes); $i < $c; $i+=4) {
    $opcode = $codes[$i];
    switch ($opcode) {
        case '1':
            $result = $codes[$codes[$i + 1]] + $codes[$codes[$i + 2]];

            $codes[$codes[$i + 3]] = $result;
            break;
        case '2':
            $result = $codes[$codes[$i + 1]] * $codes[$codes[$i + 2]];

            $codes[$codes[$i + 3]] = $result;
            break;
        case '99':
            break 2;
        default:
            echo 'wat. ' . $opcode . PHP_EOL;
            exit(1);
    }
}

echo $codes[0];