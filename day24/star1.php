<?php

$input = file_get_contents('input.txt');

$rows = explode("\n", trim($input));
$input = [];
foreach ($rows as $row) {
    $input[] = str_split($row);
}

$patterns = [];
$generation = 0;
while (true) {
    $tmp = [];
    for ($row = 0; $row < count($input); $row++) {
        $tmpRow = [];
        for ($col = 0; $col < count($input[$row]); $col++) {
            $tmpRow[] = check($row, $col, $input);
        }
        $tmp[] = $tmpRow;
    }
    $hash = md5(json_encode($tmp));
    if (isset($patterns[$hash])) {
        var_dump('Matching pattern!');
        var_dump(display($patterns[$hash]));
        var_dump('Score: ' . score($patterns[$hash]));
        break;
    }

    echo 'Generation #' . (++$generation) . ': ' . PHP_EOL . display($input);

    $patterns[$hash] = $tmp;
    $input = $tmp;
}

function display($input) {
    foreach ($input as $row) {
        foreach ($row as $cell) {
            echo $cell;
        }
        echo PHP_EOL;
    }
}

function score($input) {
    $score = 0;
    foreach ($input as $rowIdx => $row) {
        foreach ($row as $colIdx => $cell) {
            if ($cell === '#') {
                $score += pow(2, ($rowIdx * 5) + $colIdx);
            }
        }
    }
    return $score;
}

function check($row, $col, $input)
{
    list($minRow, $minCol) = [0, 0];
    list($maxRow, $maxCol) = [count($input) - 1, count($input[0]) - 1];
    list($nRow, $nCol) = [$row - 1, $col - 0];
    list($sRow, $sCol) = [$row + 1, $col + 0];
    list($wRow, $wCol) = [$row - 0, $col - 1];
    list($eRow, $eCol) = [$row + 0, $col + 1];

    $evaluate = array_filter([
        $row === $minRow ? null : [$row - 1, $col],
        $row === $maxRow ? null : [$row + 1, $col],
        $col === $minCol ? null : [$row, $col - 1],
        $col === $maxCol ? null : [$row, $col + 1],
    ]);
    $adjacent = 0;
    foreach ($evaluate as $check) {
        if ($input[$check[0]][$check[1]] === '#') {
            $adjacent++;
        }
    }
    if ($input[$row][$col] === '#' && $adjacent !== 1) {
        return '.';
    }
    if ($input[$row][$col] === '.' && ($adjacent === 1 || $adjacent === 2)) {
        return '#';
    }
    return $input[$row][$col];
}

// ..#..
// ##..#
// ##...
// #####
// .#.##
// Bug dies unless adjacent to exactly one other bug
// Empty becomes bug if adjacent to 1-2 bugs

