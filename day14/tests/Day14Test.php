<?php

use PHPUnit\Framework\TestCase;

class Day14Test extends TestCase
{
    public function setUp(): void
    {
        $this->processor = new \Forrence\AdventOfCode\ResourceProcessor;
    }

    public function test_it_passes_example_1()
    {
        // Arrange
        $input = <<<STRING
10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL
STRING;
        // Act
        $this->processor
            ->setInput($input)
            ->process();

        // Assert
        $this->assertEquals(31, $this->processor->getRequiredFuel());
    }
}