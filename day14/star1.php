<?php

$input = <<<STRING
10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL
STRING;

// Consume 7 A, 1 E to create 1 FUEL
// Consome 7 A

$mapping = [];
$lines = explode("\n", $input);
foreach ($lines as $line) {
    list($inputs, $output) = explode(" => ", $line);
    list($quantity, $label) = explode(' ', $output);
    $mapping[$label] = ['quantity' => $quantity, 'inputs' => []];
    $inputs = explode(', ', $inputs);
    $onlyUsesOre = null;
    foreach ($inputs as $input) {
        list($quantity, $inputLabel) = explode(' ', $input);
        $onlyUsesOre = $inputLabel === 'ORE' && $onlyUsesOre === null;
        $mapping[$label]['inputs'][] = [$inputLabel => $quantity];
    }
    $mapping[$label]['ore'] = $onlyUsesOre;
}

var_dump($mapping);