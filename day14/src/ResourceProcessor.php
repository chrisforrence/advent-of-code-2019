<?php

namespace Forrence\AdventOfCode;

class ResourceProcessor
{
    protected $map;

    public function __construct()
    {
        $this->map = [];
    }

    public function setInput($input)
    {
        $mapping = [];
        $lines = explode("\n", $input);
        foreach ($lines as $line) {
            list($inputs, $output) = explode(" => ", $line);
            $inputs = explode(', ', $inputs);

            $mapping[$output] = $inputs;
        }

        $this->map = $mapping;

        var_dump($this->map);

        return $this;
    }


    public function process()
    {
        return true;
    }

    public function getRequiredFuel()
    {
        return 0;
    }
}