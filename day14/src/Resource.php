<?php

namespace Forrence\AdventOfCode;

class Resource
{
    protected $label;
    protected $source;

    public function __construct($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function isFuel()
    {
        return $this->label === 'FUEL';
    }

    public function isOre()
    {
        return $this->label === 'ORE';
    }
}