<?php

use PHPUnit\Framework\TestCase;

class Day02Test extends TestCase
{
    public function setUp(): void
    {
        $this->intcode = new Forrence\AdventOfCode\Intcode;
        $this->intcode->setDebugMode(true);
    }
    public function testDay02SampleWorks()
    {
        // Arrange
        $input = file_get_contents(__DIR__ . '/input/input.day02.txt');
        $code = explode(',', $input);
        $code[1] = 12;
        $code[2] = 2;

        $this->intcode->setCode($code);

        // Act
        $output = $this->intcode->process();

        // Assert
        $this->assertEquals(3562672, $output);
    }
}