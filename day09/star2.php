<?php

require_once 'vendor/autoload.php';

use Forrence\AdventOfCode\Intcode;

$computer = new Intcode;
$computer->setCode(explode(',', file_get_contents('./input.txt')));

$computer->setDebugMode(false);
$computer->addInput(2);
$output = $computer->process();

var_dump($output);