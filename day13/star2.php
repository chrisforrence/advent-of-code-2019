<?php

require_once 'vendor/autoload.php';

use Forrence\AdventOfCode\Intcode;

$computer = new Intcode;
$code = explode(',', file_get_contents('./input.txt'));
$code[0] = 2;
$computer->setCode($code);

// $prevScore = 0;
$countBlocks = 0;

while (!$computer->isHalted()) {
    while (!$computer->isPauseForInput() && !$computer->isHalted()) {
        $computer->process();
    }
    list($ballX, $paddleX, $countBlocks) = displayBoard($computer->getOutput(), $countBlocks);
    // if ($prevScore != $score) {
    //     echo $score . PHP_EOL;
    //     echo "$prevScore + " . ($score - $prevScore) . " = $score" . PHP_EOL;
    //     $prevScore = $score;
    //     $scoreChanges[] = $score;
    // }
    $computer->addInput($ballX <=> $paddleX);
    $computer->setPauseForInput(false);
}

echo $score;

function displayBoard($output, $previousCount)
{
    $display = array_fill(0, 24, array_fill(0, 41, 0));
    $score = 0; $ballX = -1; $paddleX = -1;
    $countBlocks = 0;
    for ($i = 0, $c = count($output); $i < $c; $i += 3) {
        list($x, $y, $id) = array_slice($output, $i, 3);

        if ($x == -1 && $y == 0) {
            $score = $id;
        } else {
            $display[$y][$x] = $id;
            if ($id == 3) { $paddleX = $x; }
            else if ($id == 4) { $ballX = $x; }
        }
    }
    foreach ($display as $column) {
        foreach ($column as $cell) {
            if ($cell == 2) { $countBlocks++; }
        }
    }
    if ($countBlocks < $previousCount) {
        echo "Blocks: {$countBlocks}, Score: {$score}" . PHP_EOL;
    }

    if ($countBlocks === 0) {
        echo $score; die;
    }

    echo "\e[3J";
    for ($y = 0; $y < count($display); $y++) {
        for ($x = 0; $x < count($display[$y]); $x++) {
            echo displayChar($display[$y][$x]);
        }
        echo PHP_EOL;
    }
    echo "Score: $score" . PHP_EOL;
    return [$ballX, $paddleX, $countBlocks];
}

function displayChar($id) {
    //
    //0 is an empty tile. No game object appears in this tile.
    // 1 is a wall tile. Walls are indestructible barriers.
    // 2 is a block tile. Blocks can be broken by the ball.
    // 3 is a horizontal paddle tile. The paddle is indestructible.
    // 4 is a ball tile. The ball moves diagonally and bounces off objects.
    switch ($id) {
        case 0: return ' ';
        case 1: return '#';
        case 2: return '-';
        case 3: return '=';
        case 4: return 'o';
    }
}
function input()
{
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    return intval($line);
}