<?php

require_once 'vendor/autoload.php';

use Forrence\AdventOfCode\Intcode;

$computer = new Intcode;
$computer->setCode(explode(',', file_get_contents('./input.txt')));

while (!$computer->isHalted()) {
    $computer->process();
}

$outputs = $computer->getOutput();
$max = max($outputs);
$display = array_fill(0, $max, array_fill(0, $max, 0));
$countBlocks = 0;
for ($i = 0, $c = count($outputs); $i < $c; $i += 3) {
    list($y, $x, $id) = array_slice($outputs, $i, 3);
    $display[$y][$x] = $id;
    if ($id == 2) { $countBlocks++; }
}
var_dump($countBlocks);